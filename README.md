# Git for dummies

## Part 1: Cloning
You've probably done this before. To clone a repository, just copy it's link and preface it with `git clone` in your terminal, like so:

```
git clone https://gitsite.com/user/repo
```
this will create an exact copy of the repository. If you're working on a repository yourself, it may be more convenient to clone over ssh like this
```
git clone ssh://git@gitsite.com:user/repo
```
This will allow you to push to your repositories without putting in a password, provided you have your ssh public key shared with your git site of choosing.

## Part 2: Basic usage

Once you have made all the changes you want to your repository, you'll need to commit them. First, add the files you want to commit. A simple way to do this is with `git add .`; this will add all files within the repo that have been changed to your current commit. If you want to only add specific files, replace the . with the paths to them.

Next, make the commit itself. This is achieved with the `git commit` command. When this command is run, $EDITOR will be opened.  Go to the end of the file, create a new line, and write a short description of your commit, i.e "Added error parsing to main.c functions". (NOTE: Alternatively, this can be achieved without opening up your editor by passing the `-m` flag to `git commit`, i.e `git commit -m "Added error parsing to main.c functions."`)

Finally, once you have your commits prepared and ready, send them to the remote server. This is achieved with the `git push` command. By default, running `git push` without any arguments will try and push to the default upstream (labeled origin) on the branch you are currently in. If you want to push to another remote (provided you have one) or another branch, append the remote name and branch name to your command, i.e `git push remotename branchname`. We will cover what branches are in the next section.

To summarize: to commit changes to the repository, first add them to your commit, then create the commit, then push the commit to your remote.

```
git add .
git commit -m "Added error parsing to main.c function.'
git push
```

If someone else has made changes to the repository and you want to add them to your own local copy, run `git pull` from within the repository to get the latest changes to your branch.

## Part 3: Branches

Branches are a way of doing multiple commits to your repository without pushing them directly to the master branch, where they might be accidentally cloned without being finished, thus causing issues. This feature is useful for fixing issues or adding new features over multiple commits. To get started with branches, first create a new branch and then move to it with `git checkout`.
```
git checkout -b branchname
```
if you want to move to an existing branch, remove the -b flag from the command.

Now, you have access to a separate copy of the repository. Changes can be made and committed here, and they will not affect the master branch.

When you are ready to add the commits made to your branch to the master branch, there are a few ways you can go about merging the two branches. "Merging" a branch is the act of taking all of the changes made from within your branch and adding them to the end of another branch. Firstly, you can make a pull request on your git site of choosing. Most git sites provide this functionality, but some do not. An advantage of doing this is that commits can be easily reviewed in the same way one might review a git issue. When you make a pull request, the users who have permission to push to master (which may include yourself, but if you are working in repos you do not own you should always make pull requests instead of directly merging, unless told otherwise.) can review your request and, if they deem it acceptable, merge your commits from within the site itself.

Secondly, you can merge branches from within the terminal. I only recommend doing this if this is a sole project that you own and don't have many other people working on. To do this, checkout back to the master branch:
```
git checkout master
```
Then, merge your branch with the `git merge` command, like so:
```
git merge branchname master
```
This will essentially staple all of the commits made within that branch to the master branch.


## Addendum 1: Commit IDs
`git checkout` can be used, if you so wish, to revert you local copy back to a specific commit. To do this, run `git log`, find the commit you're wanting to revert your local copy to, and then move to it with `git checkout CommitID`. The most recent commit is also known as the HEAD commit. the commit before that is known as HEAD^. the commit before *that* is known as HEAD^1, and so on and so forth. If you want to move to a specific commit and you know which one it is relative to the head branch, you can pass it the corresponding HEAD title. i.e to checkout to the previous commit, you would use `git checkout HEAD^` and to checkout to 3 commits ago, you would use `git checkout HEAD^2`.

Congratulations, you now have a basic knowledge of how git works!
